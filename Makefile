TARGET=librand
VERSION=0.0.1

CC=gcc

all:
	@$(CC) -shared -fPIC rand.c -I. -o $(TARGET).so
	@ln -sf $(TARGET).so $(TARGET).$(VERSION).so
	@echo Built $(PWD)/$(TARGET).so

clean:
	@rm -f $(TARGET).so $(TARGET).$(VERSION).so

distclean: clean
	@rm -f librand.tar.bz2

install:
	@sudo cp -f $(TARGET).so $(TARGET).$(VERSION).so /usr/lib/
	@sudo cp -f rand.h /usr/include/
	@echo Installed /usr/lib/$(TARGET).so, /usr/lib/$(TARGET).$(VERSION).so and /usr/include/rand.h

uninstall:
	@sudo rm -f /usr/lib/$(TARGET).so /usr/lib/$(TARGET).$(VERSION).so /usr/include/rand.h
	@echo Removed /usr/lib/$(TARGET).so, /usr/lib/$(TARGET).$(VERSION).so and /usr/include/rand.h

pkg:
	@tar cvpf librand.tar rand.c rand.h Makefile
	@bzip2 -1 librand.tar
