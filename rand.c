/* rand.c: "True" random functions for linux.
 * 
 * Copyright (c) 2020 Dennis Mitten.
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see http://www.gnu.org/licenses/.
 * 
 * 
 * This version has rand32() for 32 bit and rand64() for 64 bit numbers.
 */

#include "rand.h"

static FILE *rand_fptr = NULL;

__attribute__((constructor)) void rand_init()
{
#ifdef __linux__
    rand_fptr = fopen("/dev/urandom", "r");
#endif
    if(rand_fptr == NULL)
    {
        fprintf(stderr, "Failed to init librand!\n");
        exit(1);
    }
}

int8_t rand8()
{
    if(rand_fptr == NULL)
        return -1;
    int8_t a=0;
#ifdef __linux__
    fread(&a, sizeof(a), 1, rand_fptr);
#endif
    return (a>=0?a:-a);
}

int16_t rand16()
{
    if(rand_fptr == NULL)
        return -1;
    int16_t a=0;
#ifdef __linux__
    fread(&a, sizeof(a), 1, rand_fptr);
#endif
    return (a>=0?a:-a);
}

int32_t rand32()
{
    if(rand_fptr == NULL)
        return -1;
    int32_t a=0;
#ifdef __linux__
    fread(&a, sizeof(a), 1, rand_fptr);
#endif
    return (a>=0?a:-a);
}

int64_t rand64()
{
    if(rand_fptr == NULL)
        return -1;
    int64_t a = 0;
#ifdef __linux__
    fread(&a, sizeof(a), 1, rand_fptr);
#endif
    return (a>=0?a:-a);
}

__attribute__((destructor)) void rand_quit(void)
{
    if(rand_fptr != NULL)
        fclose(rand_fptr);
}
