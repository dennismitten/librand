#ifndef RAND_H
#define RAND_H

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>

void rand_init(void);

int8_t  rand8(void);
int16_t rand16(void);
int32_t rand32(void);
int64_t rand64(void);

#endif
